resource "aws_dynamodb_table" "webservers-database" {
  name = "Users"
  billing_mode = "PROVISIONED"
  read_capacity = 20
  write_capacity = 20
  hash_key = "UserID"
  range_key = "OrderID"

  attribute {
    name = "UserID"
    type = "S"
  }

  attribute {
    name = "OrderID"
    type = "S"
  }

#  attribute {
#    name = "Email"
#    type = "S"
#  }

  tags = {
    Name = "Database-${var.environment}"
    Enviornment = "${var.environment}"
  }
}