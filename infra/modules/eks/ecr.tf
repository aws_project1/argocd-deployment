resource "aws_ecr_repository" "nginx" {
  name                 = "nginx-argocd-${var.environment}"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }

  tags = {
    Name = "ecr-nginx-${var.environment}"
  }

}