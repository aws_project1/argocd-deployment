resource "kubectl_manifest" "configmap" {
  yaml_body = <<-YAML
apiVersion: v1
kind: ConfigMap
metadata:
  name: aws-auth
  namespace: kube-system
data:
  mapUsers: |
    - userarn: arn:aws:iam::410040632229:user/Kacper_blaz
      username: Kacper_blaz
      groups:
        - system:masters
  mapRoles: |
    - groups:
      - system:bootstrappers
      - system:nodes
      rolearn: arn:aws:iam::410040632229:role/eks_node_group_role-${var.environment}
      username: system:node:{{EC2PrivateDNSName}}
  YAML
}
